'''@file                ControllerDriver.py
@brief                  Responsible for computing an error signal between reference and measured angular velocities.
@details                ControllerDriver.py receives a measured angular velocity from the encoder, a reference velocity,
                        and a value for gain, Kp. The driver outputs a value for PWM, L.

                        See documentation for main.py for link to source code.

@author                 Cole Marin
@date                   3/18/21
'''

class ClosedLoop:
    
    def __init__(self):
        
        ## gain value
        self.Kp = 1
        
        ## counter for integral controller
        self.K = 1
        
    def runP(self, omegaRef, omegaMeasured):
        '''@brief Computes error PWM, L
           @param omegaRef Reference velocity set by user
           @param omegaMeasured Current velocity reported by the encoder 
        '''
        self.L = self.Kp*(omegaRef - omegaMeasured) # in rad/s
        
        if self.L > 100:
            self.L = 100
        elif self.L < -100:
            self.L = -100
        return self.L
    def runPI(self, omegaRef, omegaMeasured, thetaRef, thetaMeasured):
        '''@brief Computes error PWM, J
           @param omegaRef Reference velocity set by user
           @param omegaMeasured Current velocity reported by the encoder 
           @param thetaRef Reference position set by user
           @param thetaMeasured Current position reported by the encoder
        '''
        self.nowJ = ((omegaRef - omegaMeasured)^2 + (thetaRef - thetaMeasured)^2) + self.lastJ
        self.lastJ = self.nowJ
        self.J = (1/self.K)*self.nowJ
        self.K += 1
        return self.J
        
    def get_Kp(self):
        '''@brief returns current value of Kp
        '''
        return self.Kp
        pass
    
    def set_Kp(self, KpDesired):
        '''@brief sets value of Kp
        '''
        self.Kp = self.KpDesired
        pass
