'''@file                UI_Task.py
@brief                  Responsible for serial communication with Encoder_Frontend and ControllerTask.py.
@details                This program recieves serial inputs from the front end and either returns a value 
                        from shares (i.e. position, delta, etc.) or runs controllerTask to obtain sampled
                        velocity data over a 30 second time period. 

                        See documentation for UI_Task.py for link to source code.
                        
@author                 Cole Marin
@date                   3/18/21
'''

from shares import shares
from pyb import UART

class UI_Task:
    
    def __init__(self):
        
        ## creates an instance of shares()
        self.share = shares()
        
        ## initializes serial communication
        self.myuart = UART(2)
        
        ## current encoder delta
        self.printDelta = 0
        
        ## current encoder position
        self.printPosition = 0
        
        ## character passed from frontend through serial
        self.in_char = ''

    def UITask(self):
        '''@brief Receives and transmits data between frontend using serial
        '''
        while True:
            if self.myuart.any() != 0:
                
                # sends entered character to ControllerTask for computation
                self.in_char = self.myuart.read().decode()
                self.getData = self.share.controller(self.in_char)
                self.myuart.write(str(self.getData).encode())
                self.in_char = ''
