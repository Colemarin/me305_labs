'''@file                shares.py
@brief                  Acts as storage for variables and quantities that are accessed by several tasks.
@details                shares.py holds data such as motor position, velocity, etc. that can be accessed by main.py, 
                        UI_Task.py, and ControllerTask.py. 

                        See documentation for shares.py for link to source code.

@author                 Cole Marin
@date                   3/18/21
'''

from ControllerTask import ControllerTask

class shares:
    
    def __init__(self):
        
        ## creates an instance of ControllerTask()
        self.ctrlTask = ControllerTask()
        
        ## current encoder position, updated through ControllerTask
        self.nowPos = 0
        
        ## current encoder delta, updated through ControllerTask
        self.nowDelta = 0
        
        ## encoder position reset, updated through ControllerTask
        self.nowSet = 0
        
    def delta(self):
        '''@brief Returns current encoder delta
        '''
        self.nowDelta = self.ctrlTask.encDelta()
        return self.nowDelta
        
    def position(self):
        '''@brief Returns current encoder position
        '''
        self.nowPos = self.ctrlTask.encPos()
        return self.nowPos
    
    def setter(self):
        '''@brief Resets encoder position to zero
        '''
        self.nowSet = self.ctrlTask.encSet()
        return self.nowSet
    
    def velocity(self):
        '''@brief Returns current encoder velocity
        '''
        self.nowVelocity = self.ctrlTask.encVel()
        return self.nowVelocity
    
    def moton(self):
        '''@brief Enables motor
        '''
        self.onMot = self.ctrlTask.moton()
        
    def motoff(self):
        '''@brief Disables motor
        '''
        self.offMot = self.ctrlTask.motoff()
        
    def controller(self, in_char):
        '''@brief Returns relevant data based on user input
           @param in_char Character passed from UI_Task to ControllerTask 
        '''
        self.returnData = self.ctrlTask.controllerTask(in_char)
        return self.returnData
    
        