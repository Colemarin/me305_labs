'''@file                main.py
@brief                  main.py is responsible for running tasks on the Nucleo board at startup
@details                This project is an example of how a P/PI controller can be applied to 
                        achieve desired outputs for a DC motor. Users can interact with the program
                        through the front end, which can be found in the source code links below.
                        
                        Links to source code - 
                        main.py: https://bitbucket.org/Colemarin/me305_labs/src/master/Lab0xFF/main.py
                        Encoder_Frontend.py: https://bitbucket.org/Colemarin/me305_labs/src/master/Lab0xFF/Encoder_Frontend.py
                        ControllerTask.py: https://bitbucket.org/Colemarin/me305_labs/src/master/Lab0xFF/ControllerTask.py
                        UI_Task.py: https://bitbucket.org/Colemarin/me305_labs/src/master/Lab0xFF/UI_Task.py
                        shares.py: https://bitbucket.org/Colemarin/me305_labs/src/master/Lab0xFF/shares.py
                        Motor_Driver.py: https://bitbucket.org/Colemarin/me305_labs/src/master/Lab0xFF/Motor_Driver.py
                        Encoder.py: https://bitbucket.org/Colemarin/me305_labs/src/master/Lab0xFF/Encoder.py
                        ControllerDriver.py: https://bitbucket.org/Colemarin/me305_labs/src/master/Lab0xFF/ControllerDriver.py
                        PracticeBackend.py: https://bitbucket.org/Colemarin/me305_labs/src/master/Lab0xFF/PracticeBackend.py
                        PracticeFrontend.py: https://bitbucket.org/Colemarin/me305_labs/src/master/Lab0xFF/PracticeFrontend.py
                        
@author                 Cole Marin
@date                   3/18/21
'''

from dataCollection import dataCollection
from UI_Task import UI_Task

if __name__ == "__main__":
    
    # creating an instance of dataCollection
    task1 = dataCollection()
    
    # creating an instance of UI_Task
    task2 = UI_Task()

    while True:
        try:
            #task1.run() # runs practice data collection program
            task2.share.ctrlTask.encoder.get_velocity()
            task2.UITask()

            
            
        except KeyboardInterrupt:
            # This except block catches "Ctrl-C" from the keyboard to end the
            # while(True) loop when desired
            print('Ctrl-c has been pressed')
            break