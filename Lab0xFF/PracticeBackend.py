'''@file                PracticeBackend.py
@brief                  Takes one of two commands from PracticeFrontend.py and returns data from an arbitrary function.
@details                This program runs commands from PracticeFrontent.py and returns requested data. A similar model is 
                        applied to the term project in UI_Task.py

                        The source code for this program can be found here:

@author                 Cole Marin
@date                   3/18/21
'''
from pyb import UART
from math import exp, sin, pi
from array import array
import utime

class dataCollection:

    state          = 0
    S0_waiting     = 0
    S1_collecting  = 1
    S2_returnArray = 2
    S3_waitState   = 3
    

    def __init__(self):
        
        self.myuart   = UART(2)
        self.in_char  = ''
        self.n        = 0
        self.values   = array('f', 600*[0])
        self.time_0   = 0
        self.time_1   = 0
    
    def run(self):
        # Check for characters, if one is received respond to the PC with a formatted
        # string        
        if self.state == 0:
            if self.myuart.any() != 0:
                # Read one character and turn it into a string
                self.in_char = self.myuart.read().decode()
                if self.in_char == 'g':
                    self.time_0 = utime.ticks_ms()
                    self.time_1 = utime.ticks_ms()
                    self.state = self.S1_collecting  
                    self.in_char = ''
    
        elif self.state == self.S3_waitState:
            self.time_1 = utime.ticks_ms()
            self.state = self.S1_collecting
            # If the character was a 'g' respond with a formatted string encoded as
            # a bytearray
        elif self.state == self.S1_collecting:
            #times = array('f', list(time/100 for time in range(0,31)))
            if utime.ticks_diff(utime.ticks_ms(), self.time_1) < 100:
                self.values[2*self.n] = self.n/10
                self.values[2*self.n+1] = exp(-(self.n/10)/10)*sin(2*pi/3*(self.n/10))  
                
            elif self.myuart.any() != 0:
                self.in_char = self.myuart.read().decode()
                if self.in_char == 's':
                    self.state = self.S2_returnArray
                    self.in_char = ''
                
            elif utime.ticks_diff(utime.ticks_ms(), self.time_0) >= 30000:
                self.state = self.S2_returnArray
                
            else:
                self.n += 1
                self.state = self.S3_waitState
                
        elif self.state == self.S2_returnArray:
            self.myuart.write(str(self.values).encode())
            self.values   = array('f', 600*[0])
            self.n = 0
            self.in_char = ''
            self.state = self.S0_waiting
        
                    