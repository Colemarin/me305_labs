'''@file                Motor_Driver.py
@brief                  Driver for the motor that uses PWM to control angular speed.
@details                The motor driver is responsible for running the motor at a variable
                        PWM that is calculated intermittently by ControllerTask.py. The driver
                        is also responsible for enabling and disbling the motor, which can be
                        accomplished through interfacing with Encoder_Frontend.

                        See documentation for Motor_Driver.py for link to source code.

@author                 Cole Marin
@date                   3/18/21
'''

import pyb

class Motor_Driver:
    
    def __init__(self):
        
        ## initializes motor sleep pin A15
        self.nsleep = pyb.Pin(pyb.Pin.cpu.A15, pyb.Pin.OUT_PP)
        
        ## initializes pin 3
        self.IN3 = pyb.Pin(pyb.Pin.cpu.B0, pyb.Pin.OUT_PP)
        
        ## initializes pin 4
        self.IN4 = pyb.Pin(pyb.Pin.cpu.B1, pyb.Pin.OUT_PP)
        
        ## initializes timer 3
        self.tim3 = pyb.Timer(3, freq = 20000)

        ## initializes timer 3 on channel 3
        self.t3ch3 = self.tim3.channel(3, pyb.Timer.PWM, pin = self.IN3)
        
        ## initializes timer 3 on channel 4
        self.t3ch4 = self.tim3.channel(4, pyb.Timer.PWM, pin = self.IN4)
        
        ## sets pin 3 to low
        self.IN3.low()
        
        ## sets pin 4 to high
        self.IN4.high()
        
        ## initializes arbitrary variable for calculating duty cycle
        self.lastDuty = 50
        
    def enable(self):
        '''@brief Enables motor and sets to an arbitrary speed
        '''
        # arbitrary PWM for startup
        self.t3ch4.pulse_width_percent(50) 
        self.nsleep.high()

    def disable(self):
        '''@brief Disables motor
        '''
        self.nsleep.low()
        
    def set_duty(self, L): # could also use J
        '''@brief Uses signal error, L, to set new motor PWM 
           @param L Signal error calculated in ControllerDriver
        '''
        self.duty = (1+L/100)*self.lastDuty
        self.t3ch4.pulse_width_percent(self.duty)
        self.lastDuty = self.duty