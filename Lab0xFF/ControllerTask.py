'''@file                ControllerTask.py
@brief                  Responsible for running tasks to interface with the three driver classes.
@details                ControllerTask.py is designed to primarily interact with the motor, encoder, and
                        controller drivers as well as compute quantities required for operation. 

                        See documentation for ControllerTask.py for link to source code.

@author                 Cole Marin
@date                   3/18/21
'''
from Encoder import Encoder
from Motor_Driver import Motor_Driver
from ControllerDriver import ClosedLoop
from array import array
from pyb import UART
import utime

class ControllerTask:
    
    def __init__(self):
        
        ## initializes serial communication
        self.myuart = UART(2)
        
        ## creating an instance of the Encoder() class
        self.encoder = Encoder()
        
        ## creating an instance of the Motor_Driver() class
        self.motorDriver = Motor_Driver()
        
        ## creating an instance of the ClosedLoop() class
        self.controller = ClosedLoop()
        
        ## character recieved by ControllerTask if user ends data collection
        self.stop = ''
        
        ## most recent encoder delta
        self.deltaEnc = 0
        
        ## most recent encoder position
        self.posEnc = 0

        ## initializes array for data collection
        self.values = array('f', 601*[0])
        
        ## count of data points in data collection
        self.n = 0
        
        ## current state of FSM
        self.state = 0
        
        ## reference position in rad
        self.thetaRef = 0
        
        ## reference angular velocity in rad/s
        self.omegaRef = 150
        
        
    def encDelta(self):
        '''@brief returns difference between last two calls to encoder.update()
        '''
        self.deltaEnc = self.encoder.get_delta()
        return self.deltaEnc
    
    def encPos(self):
        '''@brief returns current motor position in rad
        '''
        self.posEnc = self.encoder.update()
        return self.posEnc
    
    def encSet(self):
        '''@brief resets encoder position to zero
        '''
        self.encoder.set_position()
    
    def moton(self):
        '''@brief enables motor
        '''
        self.onMot = self.motorDriver.enable()

    def motoff(self):
        '''@brief disables motor
        '''
        self.offMot = self.motorDriver.disable()
    
    def encVel(self):
        '''@brief returns current angular velocity of motor in rad/s
        '''
        self.omegaMeasured = self.encoder.get_velocity()
        return self.omegaMeasured
    
    def controllerTask(self, in_char):
        '''@brief returns relevant data depending on inputted value of in_char
           @param in_char command sent by UI_Task.py from user in the front end
        '''
        while True:
            
            if self.state == 0:
            
                # returns position if 'p' is detected
                if in_char == 'p':
                    self.printPosition = self.encPos()
                    in_char = ''
                    return self.printPosition
                
                # sets encoder position if 'z' is detected
                if in_char == 'z':
                    self.encSet()
                    in_char = ''
                
                # returns delta if 'd' is detected
                if in_char == 'd':
                    self.printDelta = self.encDelta()
                    return self.printDelta
                    in_char = ''
                
                # enables motor if 'e' is detected
                if in_char == 'e':
                    self.moton()
                    return
                    in_char = ''
                
                # disables motor if 'x' is detected
                if in_char == 'x':
                    self.motoff()
                    return
                    in_char = ''
                    
                # begins data collection in state 1 if 'g' is detected    
                if in_char == 'g':
                    self.time_0 = utime.ticks_ms()
                    self.time_1 = utime.ticks_ms()
                    self.time_2 = utime.ticks_us()
                    self.state = 1
                    
            elif self.state == 1: 
                
                # P CONTROLLER
                # every 20 ms, the PWM error value, L, is computed in ClosedLoop
                # and returned to be assigned to the motor PWM in Motor_Driver
                if utime.ticks_diff(utime.ticks_us(), self.time_2) >= 20000:
                    self.L = self.controller.runP(self.omegaRef, self.encVel())
                    self.motorDriver.set_duty(self.L)
                    self.state = 3
                    
                # PI CONTROLLER
                # every 20 ms, the PWM error value, J, is computed in ClosedLoop
                # and returned to be assigned to the motor PWM in Motor_Driver
                if utime.ticks_diff(utime.ticks_us(), self.time_2) >= 20000:
                    self.J = self.controller.runPI(self.omegaRef, self.encVel(), self.thetaRef, self.encPos())
                    self.motorDriver.set_duty(self.J)
                    self.state = 3
                
                # NOTE - ONLY ONE OF THE ABOVE CONTROLLERS CAN BE USED AT ONE
                # TIME. BOTH ARE SHOWN SIMULTANEOUSLY FOR EASE OF GRADING ONLY.
                    
                # every 100 ms, a data point is appended to a list of data
                # called self.values. The benefit of having this action 
                # seperate from the controller action in the previous 'if'
                # statement is we can choose to sample data at a different
                # rate than the controller runs
                elif utime.ticks_diff(utime.ticks_ms(), self.time_1) >= 100:
                    self.values[2*self.n] = self.n/10
                    self.values[2*self.n+1] = self.encVel()
                    self.n += 1
                    self.state = 2
                
                # checks for keyboard interrupt to stop collecting data
                elif self.myuart.any() != 0:    
                    self.stop = self.myuart.read().decode()
                    if self.stop == 's':
                        self.state = 4
                
                # if 30 seconds elapses, data collection will end
                elif utime.ticks_diff(utime.ticks_ms(), self.time_0) >= 30000:
                    self.state = 4
                    
            # resets timer for data collection
            elif self.state == 2:
                self.time_1 = utime.ticks_ms()
                self.state = 1
                
            # resets timer for controller action
            elif self.state == 3:
                self.time_2 = utime.ticks_us()
                self.state = 1
                
            # this is the end condition for this FSM. Motor is disabled and 
            # values are received by UI_Task and then sent to Encoder_Frontend
            # to be formatted and displayed
            elif self.state == 4:
                self.motorDriver.disable()
                return self.values      
                self.values = array('f', 600*[0])
                self.n = 0
                in_char = ''
                self.state = 0
            
    
        
        