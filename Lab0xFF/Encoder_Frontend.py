'''@file                Encoder_Frontend.py
@brief                  Interface that allows user to communicate via serial to backend.
@details                This program allows the user to prompt the backend code for such information as
                        the motors position and delta. The user can also prompt the program to reset the 
                        encoder count, take data of the motor speed, and take an input for gain, Kp. 

                        See documentation for Encoder_Frontend.py for link to source code.
                        
@author                 Cole Marin
@date                   3/18/21
'''

import serial
import matplotlib.pyplot as plt
import keyboard

def kb_cb(key):
    """ Callback function which is called when a key has been pressed.
    """
    global last_key
    last_key = key.name

# Tell the keyboard module to respond to these particular keys only
keyboard.on_release_key("Z", callback=kb_cb)
keyboard.on_release_key("P", callback=kb_cb)
keyboard.on_release_key("D", callback=kb_cb)
keyboard.on_release_key("G", callback=kb_cb)
keyboard.on_release_key("S", callback=kb_cb)
keyboard.on_release_key("E", callback=kb_cb)
keyboard.on_release_key("X", callback=kb_cb)


def get_data():
    '''@brief Sends last_key to UI_Task via serial and waits for response
    '''
    global last_key
    if last_key is not 'g':
        ser.write(str(last_key).encode())
        return ser.readline().decode()
    
    elif last_key == 'g':
        print("Data collection has begun. Press 's' to stop collecting.\r\n")
        ser.write('g'.encode())
        timeout = 0
        
        while ser.in_waiting == 0:  
            timeout+=1
            if last_key == 's':
                print("You pressed " + last_key + ", data collection has stopped.\r\n")
                ser.write('s'.encode())
                last_key = 'g'           
                
            elif timeout > 1_000_0000:
                print('timed out')
                return None

        print("Data Collection is Complete\r\n")
        return ser.readline().decode()

last_key = None
state = 0

with serial.Serial(port='COM5',baudrate=115273,timeout=1) as ser:
    while True:
        
        if state == 0:
            print("Welcome to the frontend! Here is a list of commands.\r\n\n"\
                  "MOTOR TRACKING:\r\n"\
                  "'z' - Zeros the position of encoder 1.\r\n"\
                  "'p' - Prints the position of encoder 1.\r\n"\
                  "'d' - Prints the delta for encoder 1.\r\n"\
                  "'g' - Collects controlled encoder 1 data for 30 seconds, then generates a plot.\r\n"\
                  "'s' - Ends data collection prematurely.\r\n\n"\
                  "MOTOR CONTROL:\r\n"\
                  "'e' - Enables the motor.\r\n"\
                  "'x' - Disables the motor.\r\n")
            state = 1
        elif state == 1 and last_key is not None:    
            value = get_data()
            # prints message on front end
            if last_key == 'z':
                print("The encoder position has been zeroed\r\n")
                last_key = None
            
            # prints message on front end
            elif last_key == 'p':
                print("The position of encoder 1 is " + value + " radians.\r\n")
                last_key = None
            
            # prints message on front end
            elif last_key == 'd':
                print("The delta of encoder 1 is " + value + " radians.\r\n")
                last_key = None
            
            # prints message on front end
            elif last_key == 'e':
                print("Enabling Motor.\r\n")
                last_key = None
                
            # prints message on front end
            elif last_key == 'x':
                print("Disabling Motor.\r\n")
                last_key = None
                
            # formatting received data string to be plotted
            elif last_key == 'g':
                strippedString = value.strip()
                strippedString = strippedString.replace(']','')
                strippedString = strippedString.replace(')','')
            
                # split on the commas
                splitStrings = strippedString.split(',')
                
                
                # splits array into x and y values
                N = 2
                y_vals = []
                while N < len(splitStrings):
                    y_vals.append(float(splitStrings[N]))
                    N += 2
                
                N = 3
                x_vals = [0,]
                while N < len(splitStrings)-1:
                    x_vals.append(float(splitStrings[N]))
                    N += 2
                
                # removes extra zeros due to early termination of data collection
                while x_vals[-1] == 0:
                    x_vals.pop()
                    y_vals.pop()
                
                # plots data
                plt.plot(x_vals[1:len(x_vals)-1], y_vals[1:len(y_vals)-1])
                plt.xlabel('Time')
                plt.ylabel('Encoder Velocity (rad/s)')
                plt.title('Encoder Velocity')
                plt.show()    
                
                last_key = None
            





