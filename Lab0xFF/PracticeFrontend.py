'''@file                PracticeFrontend.py
@brief                  Interface that allows user to communicate via serial to backend.
@details                This program allows the user to prompt the backend code to return data
                        samploe from an arbritrary function. A similar idea is impletmented
                        in Encoder_Frontend.py in the following weeks.

                        The source code to this program can be found here:
                        
                        Here is the state transition diagram corresponding to this program:
                        @image html XXXXXXXXXX width=1000cm
                        
@author                 Cole Marin
@date                   3/18/21
'''
import serial
import matplotlib.pyplot as plt
import csv
import keyboard
    
def kb_cb(key):
    """ Callback function which is called when a key has been pressed.
    """
    global last_key
    last_key = key.name

keyboard.on_release_key("S", callback=kb_cb)



def getData():
    ''' Example function sends a g and waits for characters back from nucleo
        then converts the characters to a string and returns it. A timeout is
        added so it doesn't wait forever to get characters if none are sent.'''
    ser.write('g'.encode())
    timeout = 0
    print("\r\nData collection has begun. Press 's' at any time to stop collecting.\r\n")
    global last_key
    while ser.in_waiting == 0:
        timeout+=1
       
        if last_key is not None:
            print("You pressed " + last_key + ", data collection has stopped.\r\n")
            ser.write('s'.encode())
            last_key = None           
            
        if timeout > 1_000_0000:
            print('timed out')
            return None
    print("Data Collection is Complete")
    return ser.readline().decode()    

last_key = None

# The with block here will automatically close the serial port once the block 
# ends
with serial.Serial(port='COM5',baudrate=115273,timeout=1) as ser:
    while True:
        
        user_in = input("Enter 'g' to get data from the Nucleo or 'q' to quit\r\n>>")
        if user_in == 'g' or user_in == 'G':
            #newValues = ('f', [])
            newValues = getData()
            
            # Remove line endings
            strippedString = newValues.strip()
            strippedString = strippedString.replace(']','')
            strippedString = strippedString.replace(')','')
            
            # split on the commas
            splitStrings = strippedString.split(',')
            
           # print("the legnth of data is: " + len(splitStrings))
            
            N = 2
            y_vals = []
            while N < len(splitStrings):
                y_vals.append(float(splitStrings[N]))
                N += 2
                
            N = 3
            x_vals = [0,]
            while N < len(splitStrings)-1:
                x_vals.append(float(splitStrings[N]))
                N += 2
            
            while y_vals[-1] == 0:
                y_vals.pop()
                
            while x_vals[-1] == 0:
                x_vals.pop()               
                
            with open('mycsv.csv', 'w', newline='') as data:
                writing = csv.writer(data)
                writing.writerow(x_vals)
                writing.writerow(y_vals)            
            
            plt.plot(x_vals, y_vals)
            plt.xlabel('Time')
            plt.ylabel('Value')
            plt.title('Damped Sine Wave')
            plt.show()
            
            newValues = ''
            x_vals = ''
            y_vals = ''
            user_in = ''
            
        elif user_in == 'q' or user_in == 'Q':
            print('Thanks')
            break