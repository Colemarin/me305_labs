'''@file                Encoder.py
@brief                  Encoder driver that computes and records the absolute position of the motor.
@details                The primary purpose of Encoder.py is to store meaningful values regarding the position
                        of the motor. This is accomplished by summing small angular displacements at constant but
                        short time intervals.

                        See documentation for Encoder.py for link to source code.
                        
@author                 Cole Marin
@date                   3/18/21
'''

import utime
import pyb

class Encoder:
    
    def __init__(self):
        
        ## initializes TIM8
        self.timer = pyb.Timer(8, prescaler = 0, period = 65535) 
        
        ## initializes channel 1 using pin C6
        self.t4ch1 = self.timer.channel(1, pyb.Timer.ENC_AB, pin=pyb.Pin.cpu.C6)
        
        ## initializes channel 2 using pin C7
        self.t4ch2 = self.timer.channel(2, pyb.Timer.ENC_AB, pin=pyb.Pin.cpu.C7) 
        
        ## current encoder position
        self.encoderPos = 0
        
        ## last call to encoder position
        self.deltaThen = 0
        
        ## difference between last two calls to encoder position
        self.delta = 0
        
        ## position of encoder from timer - this value can over/underflow
        self.encoderNow = 0
        
        ## change in position between encoder readings
        self.change = 0
        
        ## absolute encoder position - does not overflow or underflow
        self.position = 0
        
        ## used in numerical differentiation to calculate velocity
        self.thisPosition = 0
        
        ## used in numerical differentiation to calculate velocity
        self.thatPosition = 0
        
        ## current encoder angular velocity in rad/s
        self.velocity = 0
        
        ## timer variable used in numerical differentiation to calculate velocity
        self.time_0 = 0
        
    
    def update(self):
        '''@brief Updates current encoder position when called
        '''
        self.encoderPos = self.get_position()
        return self.encoderPos
    
    def get_delta(self):
        '''@brief Returns difference in two most recent calls to update()
        '''
        if self.encoderPos != self.deltaThen:
            self.delta = self.encoderPos - self.deltaThen
            self.deltaThen = self.encoderPos
        return self.delta
    
    def get_position(self):
        '''@brief Computes current encoder position when called
        '''
        self.encoderNow = self.timer.counter()
        self.change = self.encoderNow - 32767
        self.timer.counter(32767)
        
        # following statement alleviates issue where zeroing the encoder
        # would cause it to instantly underflow by 1 tick
        if self.change == -32767:
            self.change = 0
 
        # converts ticks to radians and adds incremental change to absolute
        # position
        self.change = 2*(3.14159)*self.change/4000
        self.position -= self.change
        return self.position

    def get_velocity(self):
        '''@brief Computes current encoder velocity when called
        '''
        # simply incorporates numerical differentiation to find average 
        # velocity between two most recent calls to get_position()
        self.thisPosition = self.get_position()
        self.velocity = 1000000*(self.thisPosition - self.thatPosition)/utime.ticks_diff(utime.ticks_us(), self.time_0)
        self.thatPosition = self.thisPosition
        self.time_0 = utime.ticks_us()
        return self.velocity
        
        
    def set_position(self):
        '''@brief Resets current encoder position when called
        '''
        self.position = 0
        self.encoderPos = 0
        return self.position
    
 
        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        