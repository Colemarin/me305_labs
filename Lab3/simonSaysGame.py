'''@file                simonSaysGame.py
@brief                  Simple game that prompts users to copy the sequence of flashes they see on the Nucleo board
@details                This game forces players to use their memory to remember a series of dots and dashes
                        blinked by a LED. The program then prompts the user to input that same pattern into
                        the button on the Nucleo board. If the pattern is correct, the player will advance to the next 
                        level.
                        
                        Here is a demo video for this program: https://vimeo.com/513743599
                        
                        Here is the state transition diagram corresponding to this program:
                        @image html simonSaysSTD.jpg width=1000cm
                        
@author                 Cole Marin
@date                   2/18/21
'''
from simonSaysClass import simonSaysClass

if __name__ == "__main__":
    
    task1 = simonSaysClass()
    
    while True:
        try:
            task1.run()
            
        except KeyboardInterrupt:
            # This except block catches "Ctrl-C" from the keyboard to end the
            # while(True) loop when desired
            print('Bye Bye! See you next time.')
            break
        