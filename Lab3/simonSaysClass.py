import pyb
import utime

class simonSaysClass:
    
    S0_INIT          = 0
    S1_blinking      = 1
    S2_guessing      = 2
    S3_waiting       = 3
    S4_result        = 4
    
    def __init__(self):
        
         ## The current state of the finite state machine
        self.state = 0
    
    def switchFCN(IQR_src):
        '''@brief Adds "1" to edgeCount at rising and falling edges
           @param IQR_src Activated on both rising and falling edges of button press
        '''
        
        global switchVar
        global edgeCount
             
        edgeCount += 1
        
        if (edgeCount % 2) == 1:    # modulus used to indicate whether button is falling, rising, or not moving
            switchVar = 1           # falling edge
        else:
            switchVar = 2           # rising edge
                
    pinC13 = pyb.Pin (pyb.Pin.cpu.C13)
    ButtonInt = pyb.ExtInt(pinC13, mode=pyb.ExtInt.IRQ_RISING_FALLING, pull=pyb.Pin.PULL_NONE, callback=switchFCN)
    
    def run(self):               

        pinA5 = pyb.Pin (pyb.Pin.cpu.A5)
        tim2 = pyb.Timer(2, freq = 20000)
        t2ch1 = tim2.channel(1, pyb.Timer.PWM, pin=pinA5)
        
        print('Welcome to Simon Says! Watch the pattern closely and recreate it by pushing the blue button. You can end the game at any time by pressing ctrl-C. Press the blue button to begin.')
        
        ## Tracks rising (2) and falling (1) actions of the button
        global switchVar 
        switchVar = 0
        
        ## Total number of times a rising or falling edge is detected
        global edgeCount
        edgeCount = 0
        
        ## Counts time between button presses
        timeoutTimer = 0
        
        ## Indicates current level
        levelCount = 1
        
        ## Total number of wins
        winCount = 0
        
        ## Total numuber of losses
        loseCount = 0
        
        # Alpha-numeric to morse code conversions
        morsedict = {'A':'.-', 'B':'-...', 
                     'C':'-.-.', 'D':'-..', 'E':'.', 
                     'F':'..-.', 'G':'--.', 'H':'....', 
                     'I':'..', 'J':'.---', 'K':'-.-', 
                     'L':'.-..', 'M':'--', 'N':'-.', 
                     'O':'---', 'P':'.--.', 'Q':'--.-', 
                     'R':'.-.', 'S':'...', 'T':'-', 
                     'U':'..-', 'V':'...-', 'W':'.--', 
                     'X':'-..-', 'Y':'-.--', 'Z':'--..', 
                     '1':'.----', '2':'..---', '3':'...--', 
                     '4':'....-', '5':'.....', '6':'-....', 
                     '7':'--...', '8':'---..', '9':'----.', 
                     '0':'-----'} 
        
        
        
        while True:
            try:
                
                if self.state==self.S0_INIT:
                    
                    # three random integers are created using pyb.rng() to
                    # choose three random letters/numbers from the 
                    # dictionary above
                    
                    randomInteger_1 = pyb.rng() % len(morsedict)
                    randomInteger_2 = pyb.rng() % len(morsedict)
                    randomInteger_3 = pyb.rng() % len(morsedict)
                    
                    sequence_1 = list(morsedict.values())[randomInteger_1]
                    sequence_2 = list(morsedict.values())[randomInteger_2]  
                    sequence_3 = list(morsedict.values())[randomInteger_3]
                    
                    # these three sequences make up the three levels of the 
                    # game and are contained within the array, sequence
                    
                    sequence = [sequence_1, sequence_2, sequence_3]
                    
                    levelCount = 1

                    thatTime = utime.ticks_ms()
                           
                    # when a rising edge is detected after a button press, 
                    # the state changes to S1 and the light begins to blink
                    # according to the sequence
                
                    if switchVar==2:
                        print('Watch closely!')
                        self.state = self.S1_blinking
                                                
                        
                elif self.state==self.S1_blinking:
                    
                    char = 0       
                    
                    # for loop parses through each character in the sequence,
                    # blinking a 250 ms pulse for a dot and 750 ms pulse for a
                    # dash. Each pulse is seperated with a 250 ms pause   
                    
                    # thatTime is used as a timer, resetting after each 
                    # character in the string
                    
                    for value in ''.join(sequence[0:levelCount]):                           
                        if value == '.':
                            while utime.ticks_diff(utime.ticks_ms(), thatTime) <= 250:
                                t2ch1.pulse_width_percent(100)
                                
                            thatTime = utime.ticks_ms()
                            t2ch1.pulse_width_percent(0)
                            
                        else:                                
                            while utime.ticks_diff(utime.ticks_ms(), thatTime) <= 750:
                                t2ch1.pulse_width_percent(100)
                                
                            thatTime = utime.ticks_ms()
                            t2ch1.pulse_width_percent(0)
                            
                        while utime.ticks_diff(utime.ticks_ms(), thatTime) <= 250:
                            t2ch1.pulse_width_percent(0)
                            
                        thatTime = utime.ticks_ms()
                        
                    # once the for loop is complete, the state becomes S2 and 
                    # promts the player to input the pattern
                        
                    self.state = self.S2_guessing
                    print('Time to guess!')
                    switchVar = 0                    
                    
                elif self.state==self.S2_guessing:                  
                    t2ch1.pulse_width_percent(0)
                    
                    # for each character, the button input time is captured
                    # in pressTime and is then evaluated in S4 against
                    # what the character should be
                    
                    if char < len(''.join(sequence[0:levelCount])):
                        if switchVar == 0:
                            time_0 = utime.ticks_ms()
                            timeout = utime.ticks_diff(utime.ticks_ms(), timeoutTimer)
                            
                            # if the time between button presses, timeout,
                            # becomes greater than 1 second, the game times
                            # out and the player loses
                            
                            if timeout > 1000 and char > 0:
                                loseCount += 1
                                print('You waited too long! Current score: ' + str(winCount) + ' wins and ' + str(loseCount) + ' losses.' + "\n")
                                print('Press the blue button to try again, or exit by pressing ctrl-C.')
                                self.state=self.S0_INIT
                            
                        elif switchVar == 1:
                            pressTime = utime.ticks_diff(utime.ticks_ms(), time_0)
                            
                        elif switchVar == 2:
    
                            self.state = self.S4_result
                            switchVar = 0              
                    
                    # once the player successfully inputs the last character
                    # at the end of round 3, they win the game and winCount
                    # is increased by 1
                    
                    elif char == len(''.join(sequence[0:3])):
                        winCount += 1
                        print('You win! Current score: ' + str(winCount) + ' wins and ' + str(loseCount) + ' losses. Press the blue button to play again or exit by pressing ctrl-C.')
                        self.state = self.S0_INIT
                        
                    # once the player successfully inputs the last character at
                    # at the end of any round that isn't round 3, they are
                    # congradulated and prompted to move on to the next level
                    
                    else:
                        levelCount += 1
                        print('Well done! Press the blue button to enter level ' + str(levelCount) + '.')
                        self.state = self.S3_waiting
                        
                # state S3 purely exists so that the player can wait before
                # moving onto the next round
                        
                elif self.state==self.S3_waiting:
                        
                        if switchVar == 2:
                            self.state = self.S1_blinking
                            switchVar = 0
                
                # in S4, each button push, whose time is captured in 
                # pressTime, is evaluated against the corresponding 
                # character in the sequence
                        
                elif self.state==self.S4_result:                                                         
                   
                    if ''.join(sequence[0:levelCount])[char] == '.' and pressTime <= 300:
                        self.state=self.S2_guessing
                        timeoutTimer = utime.ticks_ms()
                        
                    elif pressTime >= 300 and pressTime <= 750:
                        self.state=self.S2_guessing
                        timeoutTimer = utime.ticks_ms()
                        
                    # if the player's input does not match the correct 
                    # character, the player loses and is prompted to play
                    # again or exit
                        
                    else:
                        loseCount += 1
                        print('Not Quite. Current score: ' + str(winCount) + ' wins and ' + str(loseCount) + ' losses.'+ "\n")
                        print('Press the blue button to try again, or exit by pressing ctrl-C.')
                        self.state=self.S0_INIT                        
                        
                    char += 1
                            
                else:
                    pass
                
                
            except KeyboardInterrupt:
                #This except block catches "Ctrl-C" from the keyboard to end the
                # while (True) loop when desired
                raise KeyboardInterrupt
                    
            