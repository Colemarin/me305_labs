'''@file        WindshieldWiper.py
@brief          Computes fibonacci number at idx.
@details        This program requires the user to input an index number,
                and the program returns the corresponding fibonacci number.
                Link to source code: https://bitbucket.org/Colemarin/me305_labs/src/master/Lab1/fibonacci.py
@author         Cole Marin
@date           1/21/21
'''

# Program takes an index and returns the corresponding fibonacci number
def fib (idx):
    '''@brief Finds the fibonacci number at the specified index.
@param idx The fibonacci index entered by the user.
@return Integer representing the calculated fibonacci number at idx.
    '''
    
    if idx == 0:
        return 0
    elif idx == 1:
        return 1
    else:
        ##@brief Variable used for incrementing in while loop.
        #
        #
        m = 2
        ##@brief Fibonacci number at index (m-1) during while loop.
        #
        #
        n1 = 0
        ##@brief Fibonacci number at index m during while loop.
        #
        #
        n2 = 1
        while m <= idx:
            n2 = n2+n1
            n1 = n2-n1
            m += 1
        return n2
    
    return 0
    
if __name__ == '__main__':
    
    idx = ''
    
    while idx != 'quit':
        idx = input('Please enter a Fibonacci index number: ')
        if idx.isdigit():
            print ('fibonacci number at index {:} is {:}.'.format(idx,fib(int(idx))))
        elif idx == 'quit':
            print('bye bye!')
        else:
            print('Please enter an integer value.')
    
    