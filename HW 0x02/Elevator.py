'''@file                Elevator.py
@brief                  Simulates an elevator moving between two floors
@details                Implements a finite state machine, shown below, to
                        simulate the behavior of an elevator
@author                 Cole Marin
@date                   1/27/21
@copyright              License Info Here
'''

import time
import random

def motor_cmd(cmd):
    '''@brief Commands the motor to move or stop
       @param cmd The command to give the motor
    '''
    
    if cmd=='up':
        print('Moving Up')
    elif cmd=='down':
        print('Moving Down')
    elif cmd=='stop':
        print('Elevator Stopped')
        
def floor_1():
    '''@brief First floor sensor
    '''
    return random.choice([True, False]) #randomly returns T or F

def floor_2():
    '''@brief Second floor sensor
    '''
    return random.choice([True, False]) #randomly returns T or F

def button_1():
    '''@brief First floor button
       @details Randomly returns true or false unless the first floor
                sensor returns true, in which case button_1 returns false
    '''
    if floor_1():
        return False
    else:
        return random.choice([True, False]) #randomly returns T or F

def button_2():
    '''@brief Second floor button
       @details Randomly returns true or false unless the second floor
                sensor returns true, in which case button_2 returns false
    '''
    if floor_2():
        return False
    else:
        return random.choice([True, False]) #randomly returns T or F
        
# Function definitions go here

#This code only runs if the script is executed as main by pressing play 
# but does not run if the script is imported as a module
if __name__ == "__main__":
    #program initialization goes here
    state = 0
    
    while True:
        try:
            if state==0:
                print('S0')
                if floor_1():
                    motor_cmd('stop') #commands motor to stop
                    state = 1
            
            elif state==1:
                print('S1')
                if button_2():
                    motor_cmd('up') #commands motor to go up
                    state = 2
            
            elif state==2:
                print('S2')
                if floor_2():
                    motor_cmd('stop') #commands motor to stop
                    state = 3
            
            elif state==3:
                print('S3')
                if button_1():
                    motor_cmd('down') #commands motor to go down
                    state = 0
            
            else:
                pass
                # program should never reach here
                
            time.sleep(0.5)
            
        except KeyboardInterrupt:
            # This except block catches "Ctrl-C" from the keyboard to end the
            print('Ctrl-c has been pressed')
            break