'''@file                BlinkingLight.py
@brief                  Allows user to toggle through various light patterns on the Nucleo board
@details                This program allows the user to toggle through light patterns such
                        as a square wave, a sine wave, and a saw-tooth wave.
                        
@author                 Cole Marin
@date                   2/4/21
'''

import pyb
import utime
import math

pinA5 = pyb.Pin (pyb.Pin.cpu.A5)
tim2 = pyb.Timer(2, freq = 20000)
t2ch1 = tim2.channel(1, pyb.Timer.PWM, pin=pinA5)

def switchFCN(IQR_src):
    '''@brief Toggles between light patterns
       @param IQR_src Activated when the user presses the button on the Nucleo board
    '''
    
    global switchVar
    switchVar = True 
    
pinC13 = pyb.Pin (pyb.Pin.cpu.C13)
ButtonInt = pyb.ExtInt(pinC13, mode=pyb.ExtInt.IRQ_FALLING, pull=pyb.Pin.PULL_NONE, callback=switchFCN)
    
if __name__ == "__main__":

    ## Controls the state of the program and is made true by pressing the button on the Nucleo board
    switchVar = False
    
    ## Each state represents a discrete light pattern
    state = 0
    
    ## Time counter beginning at program initiation
    time_0 = 0
    
    ## Time counter beginning at initiation of state 1
    time_1 = 0
    
    ## Time counter beginning at initiation of state 2
    time_2 = 0
    
    print('Welcome! Press the blue button (button B1 on the Nucleo Board) to toggle through light patterns. You may end the program at any time by pressing ctrl-C.')  
    
    while True:
        try:
            
            if state == 0:
                time_0 = utime.ticks_ms()
                t2ch1.pulse_width_percent(0)              # Light is off
                
                if switchVar == True:                     # switchVar made True from button press
                    state = 1                             # and is immediately made False after 
                    switchVar = False                     # state transition
                    print('Displaying square wave.')
            
            elif state==1:
                time_1 = utime.ticks_ms()
                timeDiff = utime.ticks_diff(utime.ticks_ms(), time_0)
                t2ch1.pulse_width_percent(100*(((500 + timeDiff) % 1000) // 500))    # causes light to blink in sqare wave
                
                if switchVar == True:
                    state = 2
                    switchVar = False
                    print('Displaying sine wave.')
            
            elif state==2:
                time_2 = utime.ticks_ms()
                timeDiff = utime.ticks_diff(utime.ticks_ms(), time_1)
                t2ch1.pulse_width_percent(50 + 50 * math.sin(timeDiff * 2 * math.pi / 10000))     # causes light to blink in sine wave
                
                if switchVar == True:
                    state = 3
                    switchVar = False
                    print('Displaying saw-tooth wave.')
                    
            elif state==3:
                time_0 = utime.ticks_ms()
                timeDiff = utime.ticks_diff(utime.ticks_ms(), time_2)
                t2ch1.pulse_width_percent(0.1 * (timeDiff % 1000))         # causes light to blink in saw-tooth pattern
                
                if switchVar == True:
                    state = 1
                    switchVar = False
                    print('Displaying square wave.')
                    
            else:
                pass            
            
        except KeyboardInterrupt:
            # This except block catches "Ctrl-C" from the keyboard to end the
            # while (True) loop when desired
            print('Bye Bye!')
            break